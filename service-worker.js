var cach_name = 'cache_admin_v3';
var urls_cach = [
    './index.html',
    './assets/css/all.css',
    './assets/img/icon/cd128.png',
    './assets/img/icon/cd144.png',
    './assets/img/icon/cd152.png',
    './assets/img/icon/cd192.png',
    './assets/img/icon/cd256.png',
    './assets/img/icon/cd512.png'
];
self.addEventListener('install', event => {         // self es lo mismo que 'this'
  
  event.waitUntil(                                  // Recibe una promesa y actúa dependiendo de su resolución
    caches.open(cach_name)                           // ... Es claro qué hace
      .then(cache => {
        return cache.addAll(urls_cach); // Una vez se abre la caché se agregan a ella todos los archivos especificados
      }).then(() => {
        return self.skipWaiting();      
      })
  )
  return self.skipWaiting();      
});
  
  
self.addEventListener('fetch', event => {                   // Escuchamos al evento 'fetch',
                                                            //  este se ejecuta siempre que se hace una solicitud HTTP (se pide o envía algo por Internet)
  event.respondWith(
    caches.open(cach_name).then(cache => {               // Abrimos la caché (en este momento ya contiene los archivos que decidimos cachear)
      return fetch(event.request).then(fetchResponse => {   // event.request es la solicitud al recurso. Contiene la URL y el método utilizado
        caches.keys().then(cacheNames => {          // Toma las caches existentes

            return Promise.all(
              cacheNames.map(cacheName => {           // Recorremos las caches exitentes
                if (cach_name !== cacheName) {     // Si la caché del recorrido no es la caché actual...  
                  return caches.delete(cacheName);    // La borramos, así conservamos únicamente la más reciente
                }
              })
            );
      
          })
        return fetchResponse;                     // Después dejamos que la solicitud siga su curso

      }).catch( _ => {                            // .catch se ejecutará cuando no se pueda hacer un 'fetch', en otras palabras,
                                                  //   cuando no se pueda completar una solicitud HTTP a Internet (offline)

        return cache.match(event.request)         // cache.match intenterá encontrar un archivo que cumpla con las características del recurso solicitado
          .then(cacheResponse => cacheResponse);  // Y después enviamos ese archivo encontrado en caché como respuesta.

      })

    })

  );

});
  
  self.addEventListener('activate', event => {   // Escuchamos al evento 'activate'
  event.waitUntil(self.clients.claim());        // El SW se registra como el worker activo para el cliente actual 

  event.waitUntil(
    caches.keys().then(cacheNames => {          // Toma las caches existentes

      return Promise.all(
        cacheNames.map(cacheName => {           // Recorremos las caches exitentes
          if (cach_name !== cacheName) {     // Si la caché del recorrido no es la caché actual...  
            return caches.delete(cacheName);    // La borramos, así conservamos únicamente la más reciente
          }
        })
      );

    })
  );
});